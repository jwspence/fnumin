#include <TFile.h>
#include <TNtuple.h>
#include <TBranch.h>
#include <TLeaf.h>
#include <TROOT.h>
#include <stdlib.h>
#include <iostream>
void skim(TString input, TString output){
	TFile *f = new TFile(input);
	TNtuple* ntread = (TNtuple*)f->Get("FASERnu");
	if(!ntread)cout << "NTuple not found" << endl;
	TBranch *b = ntread->FindBranch("row_wise_branch");
	if(!b)cout << "row_wise_branch not found" << endl;
	gROOT->cd();
	TNtuple *ntwrite = new TNtuple("ntuple","","x:y:z:iz:px:py:pz");
	int nentries = b->GetEntries();
	for(int i=0;i<nentries;i++){
		b->GetEntry(i);
		TLeaf *xleaf = b->FindLeaf("x");
		TLeaf *yleaf = b->FindLeaf("y");
		TLeaf *zleaf = b->FindLeaf("z");
		TLeaf *pxleaf = b->FindLeaf("px");
		TLeaf *pyleaf = b->FindLeaf("py");
		TLeaf *pzleaf = b->FindLeaf("pz");
		TLeaf *pdgidleaf = b->FindLeaf("pdgid");
		TLeaf *chargeleaf = b->FindLeaf("charge");
		TLeaf *izleaf = b->FindLeaf("iz");
		TLeaf *izsubleaf = b->FindLeaf("izsub");
		int nhits = xleaf->GetLen();
		int pdgid, charge, iz, izsub;
		double x, y, z, px, py, pz, p;
		cout << "Processing event " << i << endl;
		cout << "nhits = " << nhits << endl;
		int count = 0;
		for(int j=0;j<nhits;j++){
			iz = izleaf->GetValue(j);
			if(iz>100)continue;
			pdgid = pdgidleaf->GetValue(j);
			if(pdgid==13||pdgid==-13)continue;
			izsub = izsubleaf->GetValue(j);
			if(izsub!=0)continue;
			charge = chargeleaf->GetValue(j);
			if(charge==0)continue;
			px = pxleaf->GetValue(j);
			py = pyleaf->GetValue(j);
			pz = pzleaf->GetValue(j);
			p = sqrt(px*px+py*py+pz*pz);
			x = xleaf->GetValue(j);
			y = yleaf->GetValue(j);
			z = zleaf->GetValue(j);
			ntwrite->Fill(x,y,z,iz,px,py,pz);
			count++;
		}
		cout << "hits passing cuts = " << count << endl;
	}
	TFile *g = new TFile(output,"RECREATE");
	ntwrite->Write();
	g->Close();
}
