#include <TFile.h>
#include <TNtuple.h>
#include <TBranch.h>
#include <TLeaf.h>
#include <TH2D.h>
#include <TCanvas>
#include <stdlib.h>
#include <iostream>

using namespace std;
void extrapolate(){
	TFile *f = new TFile("FASERnu1-skim-1000.root");
	TNtuple *nt = (TNtuple*)f->Get("ntuple");
	if(!nt)cout << "ERROR: ntuple not found" << endl;
	TBranch *xbranch = nt->FindBranch("x");
	TBranch *ybranch = nt->FindBranch("y");
	TBranch *zbranch = nt->FindBranch("z");
	TBranch *pxbranch = nt->FindBranch("px");
	TBranch *pybranch = nt->FindBranch("py");
	TBranch *pzbranch = nt->FindBranch("pz");
	TLeaf *xleaf = nt->FindLeaf("x");
	TLeaf *yleaf = nt->FindLeaf("y");
	TLeaf *zleaf = nt->FindLeaf("z");
	TLeaf *pxleaf = nt->FindLeaf("px");
	TLeaf *pyleaf = nt->FindLeaf("py");
	TLeaf *pzleaf = nt->FindLeaf("pz");
	int nhits = xbranch->GetEntries();
	TH2D *hi = new TH2D("xiyi","yi vs xi",1000, -125,125,1000,-125,125);
	TH2D *hf = new TH2D("xfyf","yf vs xf",1000, -125,125,1000,-125,125);
	hi->GetXaxis()->SetTitle("x (mm)");
	hf->GetXaxis()->SetTitle("x (mm)");
	hi->GetYaxis()->SetTitle("y (mm)");
	hf->GetYaxis()->SetTitle("y (mm)");
	double x, y, z, px, py, pz, xi, xf, yi, yf;
	double zi = -696.5; // Beginning of FASERnu
	double zf = 603.5; // End of FASERnu
	if( !xbranch || !ybranch || !zbranch || !pxbranch || !pybranch || !pzbranch )cout << "ERROR: branches not found" << endl;
	for(int i=0;i<nhits;i++){
		xbranch->GetEntry(i);
		x = xleaf->GetValue(0);
		ybranch->GetEntry(i);
		y = yleaf->GetValue(0);
		zbranch->GetEntry(i);
		z = zleaf->GetValue(0);
		pxbranch->GetEntry(i);
		px = pxleaf->GetValue(0);
		pybranch->GetEntry(i);
		py = pyleaf->GetValue(0);
		pzbranch->GetEntry(i);
		pz = pzleaf->GetValue(0);
		xi = x + (zi - z) * px / pz;
		yi = y + (zi - z) * py / pz;
		xf = x + (zf - z) * px / pz;
		yf = y + (zf - z) * py / pz;
		hi->Fill(xi,yi);
		hf->Fill(xf,yf);

	}
	TCanvas *c = new TCanvas();
	hi->Draw();
	c->SaveAs("xiyi.png");
	hf->Draw();
	c->SaveAs("xfyf.png");
}